/*
 * Copyright 2015 The Android Open Source Project
 * Copyright (C) 2015 Freescale Semiconductor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BOOTCTLH
#define BOOTCTLH

#include <stdint.h>
#include <bootable/recovery/bootloader.h>

#define SLOT_NUM 2

#define MISC_PARTITION              "/dev/block/by-name/misc"
#define BOOTCTRL_OFFSET             offsetof(struct bootloader_message_ab, slot_suffix)
#define CRC_DATA_OFFSET             (uint32_t)(&(((TBootCtl *)0)->m_aSlotMeta[0]))

typedef struct tagSlotMeta {
    uint8_t m_BootSuc:1;
    uint8_t m_TryRemain:3;
    uint8_t m_Priority:4;
}TSlotMeta;

typedef struct tagBootCtl {
    char m_magic[4];  //"\0FSL", to assure \0 in the lowest byte, just use char array, not use fourcc.
    uint32_t m_crc;
    TSlotMeta m_aSlotMeta[SLOT_NUM];
    uint8_t m_RecoveryTryRemain;
}TBootCtl;


#endif //BOOTCTLH
